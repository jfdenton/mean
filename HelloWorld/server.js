var express = require('express');
var app = express();
var bodyParser = require('body-parser');


// used in parsing incoming json 
app.use(bodyParser.json());


// main entry point to server
// req: request (what we receive)
// res: response (what we send)
app.get('/', function(req, res) {
    console.log(req);
    res.send('Hello World!');
});


/** get endpoint 
 * 
 */
app.get('/class', function(req, res, next) {
    res.send('CS2104');
});


/** post login endpoint
 *  assumes: {
 *           "username" : "user",
 *           "password" : "pass"
 *          }
 */
app.post('/login', function(req, res, next) {
    userData = req.body;
    console.log(userData);
    res.sendStatus(200);

});

// set up port to listen to,
var port = process.env.PORT || 3000;;
app.listen(port, function () {
    console.log('app listening on port ' + port);
});

// runs app on
// http://localhost:3000/ 